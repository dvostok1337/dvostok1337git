/**
 * Задача 2.
 *
 * Дописать требуемый код что бы код работал правильно.
 * Необходимо присвоить переменной result значение true, 
 * если строка source содержит подстроку spam. Иначе — false.
 *
 * Условия:
 * - Необходимо выполнить проверку что source и spam являются типом string.
 * - Строки должны быть не чувствительными к регистру
 */

function checkSpam(source, spam) {
  let result = null;

  // РЕШЕНИЕ НАЧАЛО

  if (typeof source !== 'string' || typeof spam !== 'string') {
    throw new Error('переменные source и spam должны быть строками');
  }

  const sourceLowered = source.toLowerCase();
  const spamLowered = spam.toLowerCase();

  result = sourceLowered.includes(spamLowered);

  // РЕШЕНИЕ КОНЕЦ

  return result;
}

console.log(checkSpam('pitterXXX@gmail.com', 'xxx')); // true
console.log(checkSpam('pitterxxx@gmail.com', 'XXX')); // true
console.log(checkSpam('pitterxxx@gmail.com', 'sss')); // false

