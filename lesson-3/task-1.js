/**
 * Задача 1.
 *
 * Дописать требуемый код что бы код работал правильно.
 * Необходимо преобразовать первый символ переданной строки в заглавный.
 *
 * Условия:
 * - Необходимо проверить что параметр str является строкой
 */

function upperCaseFirst(str) {
    // str ← строка которая в нашем случае равна 'pitter' или ''

    // РЕШЕНИЕ НАЧАЛО

    if (typeof str !== 'string') {
        throw new Error('переменная str должна быть строкой');
    }
    
    if(str === '') {
        return str;
    }

    const result = str[0].toUpperCase() + str.substring(1); 

    // РЕШЕНИЕ КОНЕЦ

    return result;

}

console.log(upperCaseFirst('pitter')); // Pitter
console.log(upperCaseFirst('')); // ''
