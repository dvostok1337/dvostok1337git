/**
 * Задача 3.
 *
 * Дописать требуемый код что бы код работал правильно.
 * Необходимо проверить длину строки в переменной string.
 * Если она превосходит maxLength – заменяет конец string на ... таким образом, чтобы её длина стала равна maxLength.
 * В консоль должна вывестись (при необходимости) усечённая строка.
 *
 * Условия:
 * - Переменная string должна обладать типом string;
 * - Переменная maxLength должна обладать типом number.
 */

function truncate(string, maxLength) {

    // РЕШЕНИЕ НАЧАЛО

    if (typeof string !== 'string') {
        throw new Error('переменная string должна быть строкой');
    }

    if (typeof maxLength !== 'number') {
        throw new Error('переменная maxLength должна быть числом');
    }

    if (string.length > maxLength) {
        return string.substring(0, maxLength - 3) + '...';
    } else {
        return str;
    }

    // РЕШЕНИЕ КОНЕЦ
}

console.log(truncate('Вот, что мне хотелось бы сказать на эту тему:', 21)); // 'Вот, что мне хотел...'