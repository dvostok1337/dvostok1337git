/**
 * Напишите функцию для форматирования даты.
 * 
 * Фукнция принимает 3 аргумента
 * 1. Дата которую необходимо отформатировать
 * 2. Строка которая содержит желаемый формат даты
 * 3. Разделитель для отформтированной даты
 * 
 * Обратите внимание!
 * 1. DD день в формате — 01, 02...31
 * 2. MM месяц в формате — 01, 02...12
 * 3. YYYY год в формате — 2020, 2021...
 * 4. Строка которая обозначает формат даты разделена пробелами
 * 5. В качестве разделителя может быть передано только дефис, точка или слеш
 * 6. Генерировать ошибку если в формате даты присутствет что-то другое кроме DD, MM, YYYY
 * 7. 3-й аргумент опциональный, если он передан не был, то в качестве разделителя используется точка
*/


/////////////////////////////////// Вариант 1

// const formatDate = (date, format, delimiter) => {

//     const day = date.getDate();
//     const month = date.getMonth();
//     const year = date.getFullYear();

//     return day + '/' + monthNames[month] + '/' + year;
// };

// console.log(formatDate(new Date(2021, 10, 22), 'DD MM YYYY', '/')); // 22/10/2021
// console.log(formatDate(new Date(2021, 10, 22), 'DD MM', '.')); // 22.10
// console.log(formatDate(new Date(2021, 10, 22), 'YYYY', '.')); // 2021


/////////////////////////////////// Вариант 2

// const formatDate = (date, format, delimiter) => {

//     var day = date.getDate();
//     if (day < 10) day = '0' + day;

//     var month = date.getMonth();
//     if (month < 10) month = '0' + month;

//     var year = date.getFullYear();
//     if (year < 10) year = '0' + year;

//     return day + '/' + month + '/' + year;
// };

// console.log(formatDate(new Date(2021, 10, 22), 'DD MM YYYY', '/')); // 22/10/2021
// console.log(formatDate(new Date(2021, 10, 22), 'DD MM', '.')); // 22.10
// console.log(formatDate(new Date(2021, 10, 22), 'YYYY', '.')); // 2021
